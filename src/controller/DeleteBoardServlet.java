package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.BoardService;
import service.CommentService;


@WebServlet(urlPatterns = { "/deleteboard" })
public class DeleteBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//削除ボタンが押されたら、
		//boardsのidを使って記事を削除するよ
		BoardService boardService = new BoardService();
		boardService.delete(Integer.parseInt(request.getParameter("id")));//String型のidをparseIntしてint型へ変換


		//削除ボタンが押されたら、
		//commentのidを使って記事を削除するよ
		CommentService commentService = new CommentService();
		commentService.delete(Integer.parseInt(request.getParameter("id")));//String型のidをparseIntしてint型へ変換

		response.sendRedirect("./");
	}
}
