package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfo;
import service.UserInfoService;

@WebServlet(urlPatterns = {"/users"})
public class UserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override//ユーザーの一覧を表示
	protected void doGet (HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		List<UserInfo> userInfos = new UserInfoService().getUserInfo();
		//「userInfo」という名前をつけた箱に"userInfo"という文字列を保存
		//文字列はJSPに変数はservletへの指示書
		request.setAttribute("userInfos", userInfos);

	request.getRequestDispatcher("users.jsp").forward(request, response);
	}
}
