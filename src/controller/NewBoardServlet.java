package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Board;
import beans.User;
import service.BoardService;

@WebServlet(urlPatterns = { "/newboard" })
public class NewBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override //
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		request.getRequestDispatcher("new.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		//セッションを開始する
		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();

		User user = (User) session.getAttribute("loginUser");

		Board board = new Board();
		board.setUserId(user.getId());
		board.setTitle(request.getParameter("title"));
		board.setCategory(request.getParameter("category"));
		board.setText(request.getParameter("text"));

		//バリデーションに問題なければ
		if (isValid(request, messages) == true) {
			//登録してtopへ移動
			new BoardService().register(board);
			response.sendRedirect("./");
		} else {
			//エラーが有ればデータ保持して画面再表示
			session.setAttribute("errorMessages", messages);
			request.setAttribute("board", board);
			request.getRequestDispatcher("new.jsp").forward(request, response);
		}
	}

	//バリデーションのメッセージ一覧
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}
		if (30 < title.length()) {
			messages.add("30文字以下で件名を入力してください");
		}

		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("10文字以下でカテゴリを入力してください");
		}

		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < text.length()) {
			messages.add("1000文字以下で本文を入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}