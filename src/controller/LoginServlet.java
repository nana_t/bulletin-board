package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override//login画面を表示
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//Serviceを経由してUserオブジェクトを取得中
		LoginService loginService = new LoginService();
		User user = loginService.login(account, password);

		//取得したオブジェクトをセッションにセット
		HttpSession session = request.getSession();

		//エラーメッセージの格納先
		List<String> messages = new ArrayList<String>();


		//アカウントとパスワードがnullじゃない(=該当データがある)時、
		if (user != null) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		} else {
			messages.add("ログインに失敗しました");
			request.setAttribute("errorMessages", messages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}
}
