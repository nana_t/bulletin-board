package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override//まずは編集画面を表示(各個人の編集画面を表示)
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();

		//個別のIDを取得
		String id = request.getParameter("id");

		//idが存在しない言葉だったり数字以外だった場合(バリデーション)
		if(id == null || !(id.matches("\\d+"))){
			messages.add("IDが不正です");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("users");
			return;
		}

		User editUser = new UserService().getUser(Integer.parseInt(id));
		request.setAttribute("editUser", editUser);

		//登録データがあるかどうかの確認(バリデーション)
		if(editUser == null){
			messages.add("該当のIDがありません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("users");
			return;
		}

		//支店のセレクトボックス
		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		//部署のセレクトボックス
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("settings.jsp").forward(request, response);
		}

	@Override//DBのデータを変更するよ…の前にsessionにセット
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//支店のセレクトボックス
		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		//部署のセレクトボックス
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		List<String> messages = new ArrayList<String>();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("settings");
			}

			//loginとeditが同じ場合(自分で自分のデータを変更)は禁止、
			User loginUser = (User) session.getAttribute("loginUser");
			session.setAttribute("loginUser", loginUser);
			response.sendRedirect("users");
		} else {
			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	//ここでDBに新規データ書き込んでます
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setAccount(request.getParameter("account"));

		//パスワードが空欄だった場合は初期値（元データ）で再登録
		String password = request.getParameter("password");
		if (StringUtils.isEmpty(password) == true) {
			editUser.setPassword(editUser.getAccount());
		}else {
			editUser.setPassword(request.getParameter("password"));
		}

		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		Date formatDate = null;
		try {
			formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.getParameter("update_date"));
		} catch (ParseException e) {
			// フォーマット変換に失敗したらエラー
			e.printStackTrace();
		}

		editUser.setUpdateDate(formatDate);
		return editUser;
	}

	//バリデーション
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String id = (request.getParameter("id"));
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");

		//登録があって且つ既(editUser)IDと入力(getParameter("id"))IDが同じであった時以外の場合
		//(1)editUserを取得
		User editUser = new UserService().getUser(Integer.parseInt(id));
		request.setAttribute("editUser", editUser);
		User user = new UserService().getUser(account);

		if(user != null && !(editUser.getId() == user.getId())){
			messages.add("既に登録されているログインIDです");
		}

		if(!password.equals(password2)){
			messages.add("パスワードが一致していません");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}

		//支店、部署の組み合わせの確認
		if(branchId.equals("1")){
			if(!(positionId.equals("1") || positionId.equals("2") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("2")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("3")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("4")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
