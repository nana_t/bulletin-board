package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;


@WebServlet(urlPatterns = {"/account"})
public class AccountServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//userのidを使って停止・復活のデータに書き換えるよ
		UserService userService = new UserService();
		userService.account(Integer.parseInt(request.getParameter("id")),
				Integer.parseInt(request.getParameter("isDeleted")));

	response.sendRedirect("users");
	}
}
