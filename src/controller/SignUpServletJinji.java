package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServletJinji extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override//sigh.upを表示
	protected void doGet (HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//支店のセレクトボックス
		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		//部署のセレクトボックス
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//支店のセレクトボックス
		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches", branches);

		//部署のセレクトボックス
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("positions", positions);

		//リクエストパラメータをUserオブジェクトにセット
		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		if (isValid(request, messages) == true) {

			//バリデーションが問題なければ登録してセンドリダイレクト
			new UserService().register(signUpUser);
			response.sendRedirect("users");
		} else {
			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	//ServiceのメソッドをコールしてDBへユーザー登録
	private User getSignUpUser(HttpServletRequest request)
			throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setAccount(request.getParameter("account"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		signUpUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		return signUpUser;
	}

	//バリデーション
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");


		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}else if(!(account.matches("^[a-zA-Z0-9]{6,20}$"))) {
			messages.add("ログインIDは半角英数字で6文字以上20以下で設定してください");
		}

		//Userがnullじゃない場合は既にデータの登録がある
		User user = new UserService().getUser(account);
		if(user != null){
			messages.add("既に登録されているログインIDです");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!(password.matches("^[a-zA-Z0-9]{6,20}$"))) {
			messages.add("パスワードは半角文字で6文字以上20以下で設定してください");
		}

		if(!password.equals(password2)){
			messages.add("パスワードが一致していません");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}

		//支店、部署の組み合わせの確認
		if(branchId.equals("1")){
			if(!(positionId.equals("1") || positionId.equals("2") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("2")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("3")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}
		if(branchId.equals("4")){
			if(!(positionId.equals("3") || positionId.equals("4"))){
				messages.add("支店と部署の組み合わせが異なります");
			}
		}

		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}