package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.UpBoard;
import service.BoardService;
import service.CommentService;


@WebServlet(urlPatterns = {"/index.jsp"})
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//絞込みの検索
		//検索値（期間）の取得
		String start = request.getParameter("start");
		String end = request.getParameter("end");


		//期間が指定されなかった場合。初期値（過去～現在時刻まで）の期間で表示
		if(StringUtils.isEmpty(start)) {
			start = "2017-11-01 00:00:00";
		}else {
			start += " 00:00:00";
		}

		//Dateクラスで、現在日時を取得
		Date d = new Date();

		// 表示形式を指定 "yyyy/MM/dd HH:mm:ss"
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(d);

		if(StringUtils.isEmpty(end)) {
			end = date;
		}else {
			end += " 23:59:59";
		}

		//検索値（カテゴリ）の取得
		String category = request.getParameter("category");

		//データの照会してセッションにセット
		BoardService boardService = new BoardService();
		List<UpBoard> upBoards = boardService.getUpBoard(start, end, category);

		//値の保持
		request.setAttribute("start", request.getParameter("start"));
		request.setAttribute("end", request.getParameter("end"));
		request.setAttribute("category", category);

		//投稿の表示
		request.setAttribute("upBoards", upBoards);

		//コメントの表示
		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}
}