package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;

public class CommentService {
		public void register(Comment comment) {
			//コメントを入力しまーす
			Connection connection = null;
			try {
				connection = getConnection();

				CommentDao commentDao = new CommentDao();
				commentDao.insert(connection, comment);

				commit(connection);

			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

		private static final int LIMIT_NUM = 1000;
		//DBに入力したコメントを照会
		public List<Comment> getComment() {

			Connection connection = null;
			try {
				connection = getConnection();

				CommentDao commentDao = new CommentDao();
				List<Comment> ret = commentDao.getComment(connection, LIMIT_NUM);

				commit(connection);

				return ret;
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

		public void delete(int id) {

			Connection connection = null;
			try {
				connection = getConnection();

				CommentDao commentDao = new CommentDao();
				commentDao.delete(connection, id);

				commit(connection);

			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}
}


