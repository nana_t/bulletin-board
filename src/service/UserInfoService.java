package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserInfo;
import dao.UserInfoDao;

public class UserInfoService {
	private static final int LIMIT_NUM = 1000;
	//ユーザーの一覧取得
	public List<UserInfo> getUserInfo() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserInfoDao userInfoDao = new UserInfoDao();
			List<UserInfo> ret = userInfoDao.getUserInfo(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
