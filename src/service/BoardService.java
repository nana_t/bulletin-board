package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Board;
import beans.UpBoard;
import dao.BoardDao;
import dao.UpBoardDao;

public class BoardService {

	public void register(Board board) {

		Connection connection = null;
		try {
			connection = getConnection();

			BoardDao boardDao = new BoardDao();
			boardDao.insert(connection, board);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	//表示のService
	public List<UpBoard> getUpBoard(String start, String end, String category) {

		Connection connection = null;
		try {
			connection = getConnection();

			UpBoardDao upBoardDao = new UpBoardDao();
			List<UpBoard> ret = upBoardDao.getUpBoard(connection, LIMIT_NUM, start, end, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			BoardDao boardDao = new BoardDao();
			boardDao.delete(connection, id);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}