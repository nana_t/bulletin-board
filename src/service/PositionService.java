package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Position;
import dao.PositionDao;

public class PositionService {
	//セレクトボックス内のデータ参照
		public List<Position> getPosition() {

			Connection connection = null;
			try {
				connection = getConnection();

				PositionDao positionDao = new PositionDao();
				List<Position> ret = positionDao.getPosition(connection);
				//User user = new UserDao().getUser(connection, id);

				commit(connection);

				return ret;
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

}
