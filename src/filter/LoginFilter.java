package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override //doFilterメソッドにフィルタ処理を記載
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//セッションを取得
		HttpSession session = ((HttpServletRequest)request).getSession();

		//サーブレットパスを取得
		String servletPath = ((HttpServletRequest)request).getServletPath();

		//取得したサーブレットパスとログインサーブレットが異なる場合
		if(!servletPath.equals("/login")){
			//CSSのパスが異なる場合
			if(!servletPath.equals("/css/newBoardStyle.css")){

				//エラーメッセージのセット
				List<String> messages = new ArrayList<String>();

				//ログイン時に必要なユーザー情報がnullだった場合
				if(session.getAttribute("loginUser") == null){  //ログイン画面を表示
					messages.add("ログインしてください");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse)response).sendRedirect("login");
				}else {            //unllじゃなかったのでサーブレットに流して処理を実行（エラー文表示）
					chain.doFilter(request, response);
				}
			} else {    //cssが該当のものと等しいので流す
				chain.doFilter(request, response);
			}
		} else { //サーブレットパスと等しいのでサーブレットに流して処理を実行（ログイン処理）
			 chain.doFilter(request, response);
		}
	}

	@Override //パラメータの初期化
	public void init(FilterConfig config) throws ServletException {
	}

	@Override //保持していたインスタンスの破棄
	public void destroy() {
	}

}
