package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/users", "/settings*", "/signup"})
public class AccessFilter implements Filter {

	@Override //保持していたインスタンスの破棄
	public void destroy() {
	}

	@Override //doFilterメソッドにフィルタ処理を記載
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//セッション中のloginUserからpositonIdとbranchIdを取得する
		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		//エラーメッセージのセット
		List<String> messages = new ArrayList<String>();

		//loginUserがnull(ログインしていない)か、支店と部署が指定した数字ならば、フィルタを通す
		if(loginUser == null || loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1) {
			chain.doFilter(request, response);
		}else {
			messages.add("ログイン権限がありません。本社総務人事担当者へ連絡してください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");
		}
	}

	@Override //パラメータの初期化
	public void init(FilterConfig config) throws ServletException {
	}

}


