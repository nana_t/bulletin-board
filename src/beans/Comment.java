package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
		private static final long serialVersionUID = 1L;

		private int id;
		private int userId;
		private String name;
		private int boardId;
		private String text;
		private Date insertDate;
		private Date updateDate;



		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}

		public int getBoardId() {
			return boardId;
		}
		public void setBoardId(int boardId) {
			this.boardId = boardId;
		}

		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}

		public Date getInsertDate() {
			return insertDate;
		}
		public void setInsertDate(Date insertDate) {
			this.insertDate = insertDate;
		}

		public Date getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}
}
