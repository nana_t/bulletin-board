package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {
	//selectBox作る際のデータ取得
		public List<Position> getPosition(Connection connection) {

			PreparedStatement ps = null;
			try {
				String sql = "SELECT * FROM positions";

				ps = connection.prepareStatement(sql);

				ResultSet rs = ps.executeQuery();
				List<Position> ret = toPositionList(rs);

				return ret;
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		//参照データをリストに格納
		private List<Position> toPositionList(ResultSet rs) throws SQLException {

			List<Position> ret = new ArrayList<Position>();
			try {
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");

					Position position = new Position();
					position.setId(id);
					position.setName(name);

					ret.add(position);
				}
				return ret;
			} finally {
				close(rs);
			}
		}

}
