package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserInfo;
import exception.SQLRuntimeException;

public class UserInfoDao {

	public List<UserInfo> getUserInfo(Connection connection, int num) {
		//DBにあるデータを指定
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users_infomations");
			sql.append(" ORDER BY branch_id ");
			sql.append(", position_id ");
			sql.append(", id ");

			ps = connection.prepareStatement(sql.toString());

			//DBからデータを取得
			ResultSet rs = ps.executeQuery();
			List<UserInfo> ret = toUserInfoList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserInfo> toUserInfoList(ResultSet rs)
			throws SQLException {

		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			//値をセットセットセット～～
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String branch = rs.getString("branch");
				int positionId = rs.getInt("position_id");
				String position = rs.getString("position");
				String isDeleted = rs.getString("is_deleted");

				UserInfo userInfo = new UserInfo();
				userInfo.setId(id);
				userInfo.setAccount(account);
				userInfo.setName(name);
				userInfo.setBranchId(branchId);
				userInfo.setBranch(branch);
				userInfo.setPositionId(positionId);
				userInfo.setPosition(position);
				userInfo.setIsDeleted(isDeleted);

				ret.add(userInfo);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}