package dao;


import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Board;
import exception.SQLRuntimeException;

public class BoardDao {

	public void insert(Connection connection, Board board) {
		//入力フォームよりデータを受け取ってDBに書き込むよ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO boards ( ");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", category");
			sql.append(", text");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(") VALUES (");
			sql.append("?"); // userId
			sql.append(", ?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // insertDate
			sql.append(", CURRENT_TIMESTAMP"); // updateDate
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, board.getUserId());//userId
			ps.setString(2, board.getTitle());//title
			ps.setString(3, board.getCategory());//text
			ps.setString(4, board.getText());//category

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//データをリスト化
	public List<Board> getBoard(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM boards");
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Board> ret = toBoardList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Board> toBoardList(ResultSet rs)
			throws SQLException {

		List<Board> ret = new ArrayList<Board>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				Board board = new Board();
				board.setId(id);
				board.setUserId(userId);
				board.setTitle(title);
				board.setText(text);
				board.setCategory(category);
				board.setInsertDate(insertDate);
				board.setUpdateDate(updateDate);

				ret.add(board);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int id) {
		//入力フォームよりデータを受け取ってDBのデータを削除するよ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM boards WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}