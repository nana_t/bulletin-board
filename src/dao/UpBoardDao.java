package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UpBoard;
import exception.SQLRuntimeException;

public class UpBoardDao {

	public List<UpBoard> getUpBoard(Connection connection, int num, String start, String end, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users_boards");
			sql.append(" WHERE");
			sql.append(" insert_date");
			sql.append(" BETWEEN");
			sql.append(" ?");
			sql.append(" AND");
			sql.append(" ?");
			//もしもcategoryの値があったらSQL文実行
			if(category!=null){
				sql.append(" AND");
				sql.append(" category");
				sql.append(" LIKE ?");
			}
			sql.append("ORDER BY insert_date DESC LIMIT " + num);

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, start);
			ps.setString(2, end);
			if(category!=null){
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UpBoard> ret = getUpBoardList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UpBoard> getUpBoardList(ResultSet rs)
			throws SQLException {

		List<UpBoard> ret = new ArrayList<UpBoard>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String name = rs.getString("name");
				String branch = rs.getString("branch");
				String position = rs.getString("position");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				String text = rs.getString("text");

				UpBoard upBoard = new UpBoard();
				upBoard.setId(id);
				upBoard.setUserId(userId);
				upBoard.setTitle(title);
				upBoard.setCategory(category);
				upBoard.setName(name);
				upBoard.setBranch(branch);
				upBoard.setPosition(position);
				upBoard.setInsertDate(insertDate);
				upBoard.setText(text);

				ret.add(upBoard);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
