<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/styleSheet.css" rel="stylesheet" type="text/css">
	<title>トップページ</title>


		<script type="text/javascript">
		function check(text){
			if(window.confirm('この' + text + 'を削除してよろしいですか')){
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				return false; // 送信を中止
			}
		}
		</script>
</head>
<body>
<div id="container">
	<div id="header">
		<div class="menu">
			<a href="newboard">新規投稿</a>
			<c:if test="${loginUser.branchId ==1 && loginUser.positionId == 1}">
			/ <a href="users">管理画面</a></c:if></div>
		<div class="logout"><input type="button" onclick="location.href='logout'" value="ログアウト"></div>
	</div>
		<%-- エラーメッセージ --%>
		<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="comment">
						<li><c:out value="${comment}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	<%---ログインユーザー情報
	<div class="profile">ログインユーザー情報
		<c:if test="${ not empty loginUser }">
			<div class="name">
				<c:out value="${loginUser.name}" /></div>
			<div class="branch_id">
				<c:out value="${loginUser.branchId}" /></div>
			<div class="position_id">
				<c:out value="${loginUser.positionId}" />
			</div><br />
		</c:if>
	</div> --%>

	<div id="main-contents">
		<div class="category">
			<div class="category-box">絞り込み機能</div>
				<p><form action="./" method="get">
					<span class="word">カテゴリー</span><input name="category" id="category" value="${category}"><br />
					<span class="word">投稿日</span><input type="date" name="start" id="start" value="${start}"> ～ <input type="date" name="end" id="end" value="${end}"><br />
					<input type ="submit"  value="検索"><font size="2" ><a href="./">リセット</a></font><br />
					<br /><br /></form>
				</p>
		</div>

			<%---投稿・コメントの表示 --%>
			<c:forEach items="${upBoards}" var="upBoard">
				<%---まずは投稿の表示 --%>
				<div class="upBoard">
					<div class="title">件名：<c:out value="${upBoard.title}" /></div>
						<p>	<span class="word">カテゴリー</span><c:out value="${upBoard.category}" /><br />
							<span class="word">名前</span><c:out value="${upBoard.name}" />（<c:out value="${upBoard.branch}" />・<c:out value="${upBoard.position}" />)<br />
							<span class="word">投稿日時</span><fmt:formatDate value="${upBoard.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
							<hr class="style-one">
							<div class="text">
							<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
							<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
							<c:forEach var="s" items="${fn:split(upBoard.text,'
							')}" ><c:out value="${s}" /><br></c:forEach></div>
						</p>
							<%---投稿の削除 --%>
							<c:if test="${loginUser.id == upBoard.userId}">
							<form action="deleteboard" method="post" onSubmit="return check('投稿')">
								<input type="hidden" name="id" value="${upBoard.id}">
								<input type="submit" value="削除" >
							</form>
							</c:if>

				</div>

				<%---次にコメントの表示 --%>
				<c:forEach items="${comments}" var="comment">
				<c:if test="${upBoard.id == comment.boardId}">
					<div class="comments">
							<input type="hidden" name="id" value="${upBoard.id}">
							<input type="hidden" name="board" value="${comment.boardId}">
							<p>
								<span class="word">投稿者</span><c:out value="${comment.name}" /><br />
								<span class="word">投稿日時</span><fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								<hr>
								<div class="text">
								<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
								<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
								<c:forEach var="s" items="${fn:split(comment.text,'
								')}" ><c:out value="${s}" /><br></c:forEach></div>
							</p>

								<%---コメントの削除 --%>
								<c:if test="${loginUser.id == comment.userId}">
								<form action="deleteboard" method="post" onSubmit="return check('コメント')">
									<input type="hidden" name="id" value="${comment.id}">
									<input type="submit" value="削除" >
								</form>
								</c:if>
					</div>
				</c:if>
				</c:forEach>

				<%--- コメントの投稿 --%>
				<div class="upcomments">
					<form action="comment" method="post">
						コメント（500文字まで)<br />
						<input type="hidden" name="id" value="${upBoard.id}">
							<textarea name="text"  rows="4" cols="50">${comment.text}</textarea><br />
							<input type="submit" value="投稿">
					</form>
				</div>

			</c:forEach>


	<div id="footer">Copyright(c)Nana Tomimatsu</div>
	</div>
</div>
</body>
</html>
