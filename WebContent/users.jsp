<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/usersStyle.css" rel="stylesheet" type="text/css">
	<title>管理画面</title>

		<script type="text/javascript">
		function check(text){
			if(window.confirm('このユーザーを' + text + 'してよろしいですか')){
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				return false; // 送信を中止
			}
		}
		</script>

</head>
<body>
<div id="container">
	<div id="header">
		<a href="./">Top</a> / 	管理画面 / <a href="signup">ユーザー新規登録</a>
	</div>
		<%-- エラーメッセージの表示 --%>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

	<div id="main-contents">
	<div class="form-box">
	<div class ="table">
		<table>
			<tr>
				<th>ログインID</th>
				<th>氏名</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>停止・復活</th>
			</tr>

			<c:forEach items="${userInfos}" var="userInfo">
			<tr>
				<td>${userInfo.account}</td>
				<td><a href="settings?id=${userInfo.id}">${userInfo.name}</a></td>
				<td>${userInfo.branch}</td>
				<td>${userInfo.position}</td>
				<td><%-- loginUserとuserInfoのIDが合致した時は表示しない --%>
					<c:if test="${userInfo.id != loginUser.id}">
						<c:if test="${userInfo.isDeleted == 0}">
							<form action="account" method="post" onSubmit="return check('停止')">
								<input type="hidden" name="id" value="${userInfo.id}">
								<input type="hidden" name="isDeleted" value="1">
								<input type="submit" value="停止">
							</form>
						</c:if>
						<c:if test="${userInfo.isDeleted == 1}">
							<form action="account" method="post" onSubmit="return check('復活')">
								<input type="hidden" name="id" value="${userInfo.id}">
								<input type="hidden" name="isDeleted" value="0">
								<input type="submit" value="復活">
							</form>
						</c:if>
					</c:if></td>
			</tr>
			</c:forEach>
		</table>
	</div>
	</div>


	<div id="footer">Copyright(c)Nana Tomimatsu</div>
	</div>
</div>
</body>
</html>