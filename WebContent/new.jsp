<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/newBoardStyle.css" rel="stylesheet" type="text/css">
	<title>新規投稿画面</title>
</head>
<body>
<div id="container">
	<div id="main-contents">
		<div id="header">
			<a href="./">Top</a> / 新規投稿画面
		</div>
				<%-- エラーメッセージの表示 --%>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>

		<div class="form-box">
		<h3>新規投稿</h3>
		<form action="newboard" method="post"><br />
			件名(30文字以下)<br />
			<input type="text" name="title" value="${board.title}"><br /><br />

			カテゴリー(10文字以下)<br />
			<input type="text" name="category" value="${board.category}"><br /><br />

			本文(1000文字以下)<br />
			<textarea name="text" rows="8" cols="55" >${board.text}</textarea><br /><br />

			<input type="submit" value="投稿" /> <br />
		</form>
		</div>

	<div id="footer">Copyright(c)Nana Tomimatsu</div>
	</div>

</div>
</body>
</html>