<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/newBoardStyle.css" rel="stylesheet" type="text/css">
	<title>${editUser.name}の設定</title>

	<script type="text/javascript">
		function check(){
			if(window.confirm('情報を変更してもよろしいですか')){
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				return false; // 送信を中止
			}
		}
	</script>

</head>
<body>
<div id="container">
	<div id="main-contents">
		<div id="header">
			<a href="./">Top</a> / <a href="users">管理画面</a> / ユーザー編集<br />
		</div>
				<%-- エラーメッセージの表示 --%>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>

		<div class="form-box">
		<h3>ユーザー編集</h3>
		<form action="settings" method="post"><br />
			<input type="hidden" name="id" value="${editUser.id}">
			<input type="hidden" name="update_date" value="${editUser.updateDate}">

			<span class="label">ログインID</span>
			<input name="account" value="${editUser.account}" /><br />
			<div class="caution">(半角英数字で6文字以上20以下)</div><br />

			<span class="label">パスワード</span>
			<input name="password" type="password" id="password"/><br />
			<div class="caution">(半角文字で6文字以上20以下)</div><br />

			<span class="label">確認用パスワード</span>
			<input name="password2" type="password" id="password2"/> <br /><br />

			<span class="label">名前</span>
			<input name="name" value="${editUser.name}" id="name"/><br />
			<div class="caution">(10文字以下)</div><br />

			<%-- ログインが自分以外の場合 --%>
			<c:if test="${loginUser.id != editUser.id}">
			<span class="label">支店</span>
				<select name="branchId" >
				<c:forEach items="${branches}" var="branch" >
						<option value="${branch.id}" <c:if test="${editUser.branchId == branch.id}"> selected</c:if>>
						<c:out value="${branch.name}" /></option>
				</c:forEach>
				</select><br /><br />

				<span class="label">部署・役職</span>
				<select name="positionId">
					<c:forEach items="${positions}" var="position" >
						<option value="${position.id}" <c:if test="${editUser.positionId == position.id}"> selected</c:if>>
						<c:out value="${position.name}" /></option>
					</c:forEach>
				</select><br /><br />
			</c:if>

			<%-- ログインが自分の場合 --%>
			<c:if test="${loginUser.id == editUser.id }">
			<input type="hidden" name="branchId" value="${editUser.branchId}">
			<input type="hidden" name="positionId" value="${editUser.positionId}">
			</c:if>

			<input type="submit" value="登録" onClick="return check()" />  <br />
		</form>
		</div>


	<div id="footer">Copyright(c)Nana Tomimatsu</div>
	</div>
</div>
</body>
</html>
