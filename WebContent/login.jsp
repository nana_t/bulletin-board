<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/newBoardStyle.css" rel="stylesheet" type="text/css">
	<title>ログイン画面</title>
</head>
<body>
<div id="container">
	<div id="main-contents">
		<h3>ログイン画面</h3>
			<%-- エラーメッセージの表示 --%>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session"/>
				</c:if>
		<div class="form-box">
		<form action="login" method="post"><br />
			<span class="label">ログインID</span>
			<input name="account" id="account" value="${user.id}"/><br />

			<span class="label">パスワード</span>
			<input name="password" type="password" id="password" /><br />

			<input type="submit" value="ログイン" /><br />
		</form>
		</div>

	<div id="footer">Copyright(c)Nana Tomimatsu</div>
	</div>
</div>
</body>
</html>